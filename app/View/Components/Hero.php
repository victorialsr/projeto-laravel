<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Hero extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    public $titleHero;
    public $titleBreadcrumb;
    public $titleDate;
    public $titleFaq;
    
    public function __construct($titleHero, $titleBreadcrumb, $titleDate=false, $titleFaq=false)
    {
        $this -> titleHero = $titleHero;
        $this -> titleBreadcrumb = $titleBreadcrumb;
        $this -> titleDate = $titleDate;
        $this -> titleFaq = $titleFaq;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.hero');
    }
}
