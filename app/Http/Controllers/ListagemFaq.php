<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class ListagemFaq extends Controller
{
  public function faq(){
    $posts = Post::published()->whereIn('post_type', ['faq'])->distinct()->orderBy('menu_order')->paginate(9);
    return view('site.faq', ['posts' => $posts]);
  }
}
