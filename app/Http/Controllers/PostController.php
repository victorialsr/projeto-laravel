<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Taxonomy;

class PostController extends Controller
{
    public function blog(Request $request){
      $category = Taxonomy::where('taxonomy', 'categorias-blog')->with('posts')->get();
    if($request -> search){
      $busca =  $request -> search;
      $posts = Post::where([
        ['post_title', 'like', '%' . $busca . '%']
    ])->where('post_type', '=', 'blog')->distinct()->paginate(6)->withQueryString();
      return view('site.blog', ['categoria' => $category, 'posts' => $posts]);
    }
    else if($request -> select){
      $busca = $request -> select;
      $posts = Post::Taxonomy('categorias-blog', $busca)->paginate(6)->withQueryString();
      return view('site.blog', ['categoria' => $category, 'posts' => $posts]);
    }
    else{
       $posts = Post::published()->whereIn('post_type', ['blog'])->distinct()->orderBy('menu_order')->paginate(9);
      return view('site.blog', ['posts' => $posts, 'categoria' => $category]);
    }
  }

    public function postsIndex(){
      $postsBlog = Post::published()->whereIn('post_type', ['blog'])->distinct()->orderBy('menu_order')->paginate(5);
      $postsEstabelecimentos = Post::published()->whereIn('post_type', ['descontos'])->distinct()->orderBy('menu_order')->paginate(9);
      return view('site.home', ['postsEstabelecimentos' => $postsEstabelecimentos, 'postsBlog' => $postsBlog]);
    }

    public function blogIntegra($id){
      $outrosPosts = Post::published()->whereIn('post_type', ['blog'])->Where('ID', "!=", $id)->limit(3)->get();
      $post = Post::find($id);
      return view('site.blog-integra',['post' => $post, 'outrosPosts' => $outrosPosts] );
     
    }

    public function titleIntegra($id){
      $post = Post::find($id); 
      return view('components.hero',['post' => $post]);
    }

    public function descontosIndex(){
      $posts = Post::published()->whereIn('post_type', ['descontos'])->distinct()->orderBy('menu_order')->paginate(3);
      return view('site.home', ['posts' => $posts]);
    }
}
