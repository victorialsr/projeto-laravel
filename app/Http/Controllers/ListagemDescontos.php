<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Taxonomy;

class ListagemDescontos extends Controller
{
  public function descontos(Request $request){
    $category = Taxonomy::where('taxonomy', 'categorias-descontos')->with('posts')->get();
    if($request -> search){
      $busca =  $request -> search;
      $posts = Post::where([
        ['post_title', 'like', '%' . $busca . '%']
    ])->where('post_type', '=', 'descontos')->distinct()->paginate(6)->withQueryString();
      return view('site.descontos', ['categoria' => $category, 'posts' => $posts]);
    }
    else if($request -> select){
      $busca = $request -> select;
      $posts = Post::Taxonomy('categorias-descontos', $busca)->paginate(6)->withQueryString();
      return view('site.descontos', ['categoria' => $category, 'posts' => $posts]);
    }
    else{
      $posts = Post::published()->whereIn('post_type', ['descontos'])->distinct()->orderBy('menu_order')->paginate(9);
      return view('site.descontos', ['posts' => $posts, 'categoria' => $category]);
    }
   
  }

  public function descontoIntegra($id){
    $post = Post::find($id);
    return view('site.descontos-integra', ['post' => $post] );
  }


}