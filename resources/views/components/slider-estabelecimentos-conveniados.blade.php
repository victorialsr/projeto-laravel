
  <div class="swiper swiperEstabelecimentosConveniados">
    <div class="swiper-wrapper">
      <div class="swiper-slide">
        <div class="card">
          <a href="/blog" class="img-container">
            <img src="site/img/img-conveniados-1.png" alt="">
            <p>10%</p>
          </a>
          <div class="content">
            <h3><a href="">Madê Cozinha autoral</a> </h3>
            <p><a href="">Um restaurante simples, requintado e com muito sabore qualidade.</a></p>
            <a class="outro">+</a>
          </div>
        </div>
		  </div>
      <div class="swiper-slide">
        <div class="card">
          <a href="/blog" class="img-container">
            <img src="site/img/img-conveniados-1.png" alt="">
            <p>10%</p>
          </a>
          <div class="content">
            <h3><a href="">Madê Cozinha autoral</a> </h3>
            <p><a href="">Um restaurante simples, requintado e com muito sabore qualidade.</a></p>
            <a class="outro">+</a>
          </div>
        </div>
		  </div><div class="swiper-slide">
        <div class="card">
          <a href="/blog" class="img-container">
            <img src="site/img/img-conveniados-1.png" alt="">
            <p>10%</p>
          </a>
          <div class="content">
            <h3><a href="">Madê Cozinha autoral</a> </h3>
            <p><a href="">Um restaurante simples, requintado e com muito sabore qualidade.</a></p>
            <a class="outro">+</a>
          </div>
        </div>
		  </div>
    </div>
    <div class="buttons-container">
      <div class="swiper-button-next"></div>
      <div class="swiper-button-prev"></div>
    </div>
   
    <div class="swiper-pagination"></div>
  </div>
</div>