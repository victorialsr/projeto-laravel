<section {{$attributes->merge(['class' => 'section-hero-component'])}}>
  <div class="container">
	<div class="hero-component-content">
    @if ($titleDate ?? false)
      <p class="date-blog">{{$titleDate ?? ''}}</p>
    @endif
		<h1>{{ $titleHero }}</h1>
    @if ($titleFaq ?? false)
      <h2>Dúvidas Frequentes</h2>
    @endif
    <div class="breadcrumb">
		    <div class="container">
          <nav>
            <ul>
              <li><a href="">Home</a></li>
              <li>/</li>
              <li><a href="">{{ $titleBreadcrumb }}</a></li>
            </ul>
          </nav>
		    </div>
	    </div>
    </div>
</div>
</div>
</section>