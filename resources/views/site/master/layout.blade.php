<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700&display=swap" rel="stylesheet">
	<link
  rel="stylesheet"
  href="https://unpkg.com/swiper@7/swiper-bundle.min.css"/>	
  <link href="https://cdn.jsdelivr.net/npm/glider-js@1/glider.min.css" rel="stylesheet">

  <link rel="stylesheet" href="{{ url(mix('site/css/style.css')) }}">
  <title>Projeto Laravel - @yield('title')</title>
</head>
</head>
<body>
<header>
    <div class="header-top">
			<div class="container">
				<div class="header-top-content">
					<p>27° | 22° Tempo</p> 	
					<div class="header-icons">
						<a href="">	<img src="/site/img/icon-facebook.svg" alt=""></a>
						<a href="">	<img src="/site/img/icon-instagram.svg" alt=""></a>
					</div>
				</div>
			</div>
		</div>
		<div class="header-main">
			<div class="container">
				<div class="header-main-content">
					<div class="logo-container">
            <a href="/"><img src="/site/img/logo-clube.png" alt=""></a>
					</div>
					<nav class="nav-menu">
						<button id="btn-mobile">
							<span id="hamburguer"></span>
						</button>
						<ul class="menu-list">
              <div class="menu">	
                <li class="menu-link {{ (Route::current()->getName() === 'site.descontos' ? 'active' : '') }}"><a href="{{ route('site.descontos') }}">Descontos</a></li>
                <li class="menu-link {{ (Route::current()->getName() === 'site.faq' ? 'active' : '') }}"><a href="{{ route('site.faq') }}">Faq</a></li>
                <li class="menu-link {{ (Route::current()->getName() === 'site.parceiros' ? 'active' : '') }}"><a href="{{ route('site.parceiros') }}">Seja Parceiro</a></li>
                <li class="menu-link {{ (Route::current()->getName() === 'site.sobre' ? 'active' : '') }}"><a href="{{ route('site.sobre') }}">Sobre</a></li>
                <li class="menu-link {{ (Route::current()->getName() === 'site.contato' ? 'active' : '') }}"><a href="{{ route('site.contato') }}">Ajuda</a></li>
              </div>
              <div class="buttons-container">
						    <div class="buttons-clube-container"> 
							    <a href="" class="btn --clubeHeader">Faça parte do clube</a>
							    <a href="" class="btn --assineHeader">Assine aqui</a>
						    </div>
						    <div class="button-login-container">
							    <a href="" class="btn --login">
								    <img src="/site/img/icon-profile.svg" alt="">
								    <span>Entrar</span>
							    </a>
						    </div>
					    </div>
						</ul>
					</nav>
				</div>
			</div>
		</div>
  </header>
	<main role="main">
    @yield('content')
 	</main>
   <footer>
		<div class="footer-top">
			<div class="footer-top-content container">
				<h3>Lorem ipsum dolor sit amet, consectetur.</h3>
				<a href="#" class="btn --clubeFooter">Lorem ipsum dolor sit amet</a>
			</div>
		</div>
		<div class="footer-main">
			<nav class="nav-footer container">
				<div class="col">
					<ul>
						<h3>Anuncie</h3>
						<li><a href="">Anúncios Web</a></li>
						<li><a href="">Midiaweb</a></li>
					</ul>
					<ul>
						<h3>Assinatura</h3>
						<li><a href="">Clube AT</a></li>
						<li><a href="">Edição Digital</a></li>
					</ul>
					<ul>
						<h3>Classificados</h3>
						<li><a href="">Diversos</a></li>
						<li><a href="">Empregos</a></li>
						<li><a href="">Imóveis</a></li>
						<li><a href="">Veículos</a></li>
					</ul>
					<h3>Expediente</h3>
					<h3>Política de Privacidade</h3>
					<h3>Termos de Uso</h3>
				</div>
				<div class="col">
					<ul>
						<h3>últimas Notícias</h3>
						<li><a href="">Anúncios Web</a></li>
						<li><a href="">Atualidades</a></li>
						<li><a href="">Cidades</a></li>
						<li><a href="">Ciência & Saúde</a></li>
						<li><a href="">Concurso & Emprego</a></li>
						<li><a href="">Economia</a></li>
						<li><a href="">Eleições 2020</a></li>
						<li><a href="">Natureza</a></li>
						<li><a href="">Passei por aqui</a></li>
						<li><a href="">Polícia</a></li>
						<li><a href="">Política</a></li>
						<li><a href="">Porto & Mar</a></li>
						<li><a href="">Turismo</a></li>
						<li><a href="">Vc na AT</a></li>
					</ul>
				</div>
				<div class="col">
					<ul>
						<h3>Cidades </h3>
						<li><a href="">Bertioga</a></li>
						<li><a href="">Cubatão</a></li>
						<li><a href="">Guarujá</a></li>
						<li><a href="">Litoral Sul</a></li>
						<li><a href="">Praia Grande</a></li>
						<li><a href="">Santos</a></li>
						<li><a href="">São Vicente</a></li>
						<li><a href="">Vale do Ribeira</a></li>
					</ul>
					<ul>
						<h3>Variedades</h3>
						<li><a href="">AT Revista</a></li>
						<li><a href="">Boa Mesa</a></li>
						<li><a href="">Comportamento</a></li>
						<li><a href="">Edição Digital</a></li>
						<li><a href="">Games & Tecnologia</a></li>
						<li><a href="">Luiz Alca</a></li>
						<li><a href="">Programe-se</a></li>
						<li><a href="">Pop & Art</a></li>
					</ul>
				</div>
				<div class="col">
					<ul>
						<h3>Esportes</h3>
						<li><a href="">Corinthians</a></li>
						<li><a href="">Jabaquara</a></li>
						<li><a href="">Santos FC</a></li>
						<li><a href="">São Paulo</a></li>
						<li><a href="">Palmeiras</a></li>
						<li><a href="">Portuguesa Santista</a></li>
						<li><a href="">Esporte regional</a></li>
						<li><a href="">Mais esporte</a></li>
					</ul>
					<ul>
						<h3>Especiais</h3>
						<li><a href="">AT Ciência</a></li>
						<li><a href="">AT Imóveis</a></li>
						<li><a href="">Nossa Senhora do M. Serrat</a></li>
						<li><a href="">Onde ir</a></li>
						<li><a href="">Revista Porto & Mar</a></li>
						<li><a href="">Porto & Mar</a></li>
						<li><a href="">Saúde AT</a></li>
						<li><a href="">Top of Mind </a></li>
					</ul>
				</div>
				<div class="col">
					<ul>
						<h3>Eventos</h3>
						<li><a href="">10 KM Tribuna FM-Unilus</a></li>
						<li><a href="">A Região em Pauta</a></li>
						<li><a href="">A Tribuna de Surf Colegial</a></li>
						<li><a href="">A Tribuna de Tênis</a></li>
						<li><a href="">Arena Praia & Cia</a></li>
						<li><a href="">Atitude Verde</a></li>
						<li><a href="">Comunidade em Ação</a></li>
						<li><a href="">Congresso de Direito</a></li>
						<li><a href="">Marítimo e Portuário</a></li>
						<li><a href="">Corrida Virtual</a></li>
						<li><a href="">Envelheça Leve</a></li>
						<li><a href="">Porto Profissões</a></li>
						<li><a href="">Seminário da Indústria da Construção Civil</a></li>
						<li><a href="">Seminário Porto & Mar</a></li>
						<li><a href="">Top of Mind</a></li>
						<li><a href="">Tribuna Motor Show</a></li>	
					</ul>
				</div>
				<div class="col">
					<ul>
						<h3>Anuncie</h3>
						<li><a href="">	Alexandre Catena Volpe</a></li>
						<li><a href="">Alexandre Lopes</a></li>
						<li><a href="">Ana Lucia M. Simão Cury</a></li>
						<li><a href="">Arminda Augusto</a></li>
						<li><a href="">Blog n'Roll</a></li>
						<li><a href="">Caio França</a></li>
						<li><a href="">Cida Coelho</a></li>
						<li><a href="">Dad Squarisi</a></li>
						<li><a href="">Direito Previdenciário</a></li>
						<li><a href="">Editorial A Tribuna</a></li>
						<li><a href="">Eduardo Silva</a></li>
						<li><a href="">Eu Estudo Certo</a></li>
						<li><a href="">Júnior Bozzella</a></li>
						<li><a href="">Kenny Mendes</a></li>
						<li><a href="">Márcia Atik</a></li>
						<li><a href="">Márcio Calves</a></li>
						<li><a href="">Maxwell Rodrigues</a></li>
						<li><a href="">Paulo Corrêa Jr.</a></li>
						<li><a href="">Resenha Esportiva</a></li>
						<li><a href="">Roberto Monteiro</a></li>
						<li><a href="">Rosana Valle</a></li>
						<li><a href="">Tenente Coimbra</a></li>
						<li><a href="">Tribuna do leitor</a></li>
					</ul>
				</div>
			</nav>
		</div>
		<div class="footer-newsletter">
			<div class="footer-newsletter-content container">			
				<div class="social-media">
					<div class="logo"> 
					<img src="/site/img/logo-footer.png" alt="">
						<a href="" class="btn --assine">Assine</a>
					</div>
					<div class="icons-container">
						<p>Acompanhe-nos</p>
						<div class="icons-img-container">
							<div class="icon-img-container">
								<img src="/site/img/icon-facebook-footer.svg" alt="">
							</div>
							<div class="icon-img-container">
								<img src="/site/img/icon-instagram-footer.svg" alt="">
							</div>
							<div class="icon-img-container">
								<img src="/site/img/icon-twitter-footer.svg" alt="">
							</div>
							<div class="icon-img-container">
								<img src="/site/img/icon-youtube-footer.svg" alt="">
							</div>						
						</div>	
					</div>
				</div>
				<div class="form-newsletter">
					<form action="">
            <div class="container-label">
              <img src="/site/img/icon-email-footer.svg" alt="">
						  <label for="newsletter">Newsletter</label>
            </div>
						<div class="container-input">
							<input type="mail" placeholder="Digite seu e-mail">
							<button>Ok</button>
						</div>	
					</form>
				</div>
			</div>
		</div>
	</footer>  	
  <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/glider-js@1/glider.min.js"></script>
	<script src="{{ url(mix('site/js/script.js')) }}"></script>
  <script id="__bs_script__">//<![CDATA[
    document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.27.7'><\/script>".replace("HOST", location.hostname));
//]]></script>

</body>
</html>