@extends('site.master.layout')
@section('title', 'Seja Parceiro')

@section('content')
<x-hero class="bg-black --hero" titleHero="Parceiros" titleBreadcrumb="Parceiros"></x-hero>
<section class="section-parceiros">
	<div class="parceiro">
		<div class="form-parceiros-wrapper">
			<h2>Faça o </h2>	
			<span>pré-cadastro</span>
			<form action="" class="form-parceiros">	
				<label for="">Empresa:</label>
				<input type="text" name="" id="">
				<div class="row-form">
					<div class="row-responsavel">
						<label for="">Responsável:</label>
						<input type="text" name="" id="">
					</div>
					<div class="row-telefone">
						<label for="">Telefone:</label>
						<input type="tel" name="" id="">
					</div>
				</div>
				<label for="">E-mail:</label>
				<input type="email" name="" id="">
				<label for="">Mensagem:</label>
				<textarea name="" id="" cols="30" rows="10"></textarea>
        <div class="button-container">
          <button class="btn --submit">Enviar</button>
        </div>
			</form>
		</div>
	</div>
</section>
@endsection