@extends('site.master.layout')

@section('title', 'Blog')

@section('content')
<x-hero class="bg-purple --hero" titleHero="Blog" titleBreadcrumb="Blog"></x-hero>
<section class="section-listagem-blog">
  <div class="container listagem-blog-content">
		<div class="title-wrapper">
			<h2 class="title">Acompanhe as notícias do clube</h2>
			<form action="/blog" method="GET">
				<div class="filter-wrapper">
					<img src="site/img/icon-filter.svg" alt="">	
					<label for="search">O que você procura</label>		
          <input type="text" name="search" placeholder="Buscar">
					<button type="submit" id="searchsubmit">Ok</button>
				</div>
				<div class="select-wrapper">
          <select name="select" id="">
            <option selected disabled>Categoria</option>
            @foreach($categoria as $itemCategory)
            <option value="{{$itemCategory->name}}">{{$itemCategory->name}}</option>
          @endforeach 
					</select>
				</div>
			</form>
		</div>
    <div class="component-listagem-blog">
      <div class="listagem-blog">
        @foreach ($posts as $post)
        <div class="card-blog">
          <div class="img-card-blog-container">
            <a href="/blog-integra/{{$post->ID}}" >
              <img src="{{$post->image}}" alt="{{$post->post_title}}">
            </a>	
          </div>
          <div class="card-blog-content">
            <p class="card-blog-data">{{$post->post_date->format('d/m/Y')}}</p>
            <h3>{{$post->post_title}}</h3>
            <a href="/blog-integra/{{$post->ID}}"class="btn" >+</a>
          </div>
        </div>
        @endforeach
        {{ $posts->links() }}
      </div>
    </div>
  </div>
</section>


@endsection