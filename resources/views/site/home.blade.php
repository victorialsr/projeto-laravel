@extends('site.master.layout')

@section('content')
<section class="section-hero">
	<div class="descontos-home">
		<div class="descontos container">
			<h3>O maior clube de vantagens da região</h3>
			<h2>Com o que você <span class="span-test">gasta</span> no mês?</h2>
			<div class="descontos-wrapper">
				<div class="desconto-wrapper">
					<img src="site/img/icon-restaurant.svg" alt="">
          <div class="desconto-content">
            <div class="desconto-number">
              <span>R$ 100</span>
              <span>20%</span>
              <span>= R$ 20</span>
            </div>
            <div class="desconto-text">
              <span>Restaurante</span>
              <span>Desconto do clube</span>
              <span>De economia</span>
            </div>
          </div>
				</div>
				<div class="desconto-wrapper">
					<img src="site/img/icon-gaspump.svg" alt="">
          <div class="desconto-content">
            <div class="desconto-number">
              <span>R$200,00</span>
              <span>5%</span>
              <span>=  R$ 10</span>
            </div>
            <div class="desconto-text">
              <span>Gasolina</span>
              <span>Desconto do clube</span>
              <span>De economia</span>
            </div>
          </div>
				</div>
				<div class="desconto-wrapper">
					<img src="site/img/icon-washing-machine.svg" alt="">
          <div class="desconto-content">
            <div class="desconto-number">
              <span>R$ 100</span>
              <span>20%</span>
              <span>= R$ 20</span>
            </div>
            <div class="desconto-text">
              <span>Lavanderia</span>
              <span>Desconto do clube</span>
              <span>De economia</span>
            </div>
          </div>
				</div>
			</div>
			<a href="" class="btn --clubeHome">Clique aqui e faça parte do clube</a>
			<p>Sua economia pode pagar a sua assinatura</p>
		</div>
	</div>
</section>

<section class="section-descontos">	
	<div class="text-descontos">
		<div class="container text-desconto-container">
			<img src="site/img/icon-coupon.svg" alt="">
			<div class="text-container">	
				<h3>Tenha acesso a</h3>
				<span>Descontos exclusivos</span>
			</div>
		</div>
	</div>
	<div class="teste-black">
		<div class="col col-black">	
		
		</div>
		<div class="col"></div>
		<div class="col"></div>
		<div class="col"></div>
		
	</div>
	<div class="teste">
		<div class="descontos-main container">
		<div class="cards-descontos">
			<div class="card-descontos left">
        <div class="img-container">
          <a href="descontos?select=gastronomia&search=">
             <img src="site/img/icon-serve.svg" alt="">
          </a>
        </div>
        <div class="card-content">
          <a href="descontos?select=gastronomia&search=">
            <p>Gastronomia</p>
            <p class="hidden">Conheça os descontos</p>
          </a>
        </div>
			</div>
			<div class="card-descontos left">
        <div class="img-container">
			    <a href="descontos?select=casa-e-decoracao&search=">
            <img src="site/img/icon-flower.svg" alt="">
          </a>
        </div>
        <div class="card-content">
          <a href="descontos?select=casa-e-decoracao&search=">
            <p>Casa e decoração</p>
            <p class="hidden">Conheça os descontos</p>
          </a>
        </div>
			</div>
			<div class="card-descontos left">
        <div class="img-container">
          <a href="descontos?select=entretenimento&search=">
            <img src="site/img/icon-popcorn.svg" alt="">
          </a>
        </div>
        <div class="card-content">
          <a href="descontos?select=entretenimento&search=">
            <p>Entretenimento</p>
            <p class="hidden">Conheça os descontos</p>
          </a>
        </div>
			</div>
		</div>
		<div class="img-wrapper">
			<img src="site/img/img-phone.png" alt="">
			<a href="" class="btn --clubeDescontos">Faça parte do clube</a>
		</div>
		<div class="cards-descontos">
			<div class="card-descontos right">
        <div class="img-container">
          <a href="descontos?select=moda-e-beleza&search=">
            <img src="site/img/icon-fashion.svg" alt="">
          </a>
        </div>
				<div class="card-content">
          <a href="descontos?select=moda-e-beleza&search=">
            <p>Moda e beleza</p>
            <p class="hidden">Conheça os descontos</p>
          </a>
        </div>
			</div>
			<div class="card-descontos right">
        <div class="img-container">	
          <a href="descontos?select=saude-e-bem-estar&search=">
            <img src="site/img/icon-heart.svg" alt="">
          </a>
        </div>
        <div class="card-content">	
          <a href="descontos?select=saude-e-bem-estar&search=">
            <p>Saúde e bem estar</p>
            <p class="hidden">Conheça os descontos</p>
          </a>
        </div>
			</div>
			<div class="card-descontos right">
        <div class="img-container">			
          <a href="descontos?select=servicos&search=">
            <img src="site/img/icon-technical-support.svg" alt="">
          </a>
        </div>
        <div class="card-content">
          <a href="descontos?select=servicos&search=">
            <p>Serviços</p>
            <p class="hidden">Conheça os descontos</p>
          </a>
        </div>
			</div>
		</div>
	</div>
</section>

<section class="section-slider-estabelecimentos-conveniados">
  <div class="container">
    <div class="title-wrapper --slider">
      <h2 class="title">Estabelecimentos <span>Conveniados</span></h2>
      <form action="/descontos" method="GET" class="--slider">
        <div class="filter-wrapper">
          <input type="text" name="search" placeholder="Buscar">
          <button type="submit" id="searchsubmit">
          </button>
        </div>
      </form>
    </div>
    <div class="component-listagem-estabelecimentos-conveniados --home">
      <div class="listagem-estabelecimentos-conveniados --home">
      <div class="swiper-container">
        <div class="swiper swiperEstabelecimentosConveniados ">
          <div class="swiper-wrapper">
            @foreach ($postsEstabelecimentos as $post)
            <div class="swiper-slide">
              <div class="card-estabelecimentos --slider">
                <a href="/desconto-integra/{{$post->ID}}" class="img-container --home">
                  <img src="{{$post->image}}" alt="{{$post->post_title}}">
                  <p>{{$post->acf->desconto}}%</p>
                </a>
                <div class="content --home">
                  <h3><a href="/desconto-integra/{{$post->ID}}">{{$post->post_title}}</a> </h3>
                  <p><a href="/desconto-integra/{{$post->ID}}">{{$post->post_excerpt}}</a></p>
                </div>
                <a href="/desconto-integra/{{$post->ID}}" class="outro">+</a>
              </div>
            </div>
            @endforeach
          </div>
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
      </div>
    </div>
  </div>
  </div>
</section>

<section class="section-vantagens">
  <div class="vantagens">
    <div class="sobre-main"> 
      <div class="container">
        <div class="onda"></div>
      </div>
      <div class="container content-sobre-main">
        <img src="site/img/logo-clube.png" alt="">
        <div class="text-sobre-main">
          <h3>O Clube</h3>
          <p>O Clube é um programa de relacionamentos com os Assinantes A Tribuna que oferece diversos benefícios para os conveniados A Tribuna.</p>
          <p>Além de muita informação, os assinantes recebem condições exclusivas nas maiores empresas da região além de descontos em shows e espetáculos de teatro.</p>
        </div>
      </div>
    </div>
    <div class="sobre-vantagens container">
      <div class="content-sobre-vantagens">
        <div class="title-sobre-vantagens">	
            <img src="site/img/icon-check.svg" alt="">
            <h3><span>Vantagens</span> de ser do clube</h3>
        </div>
        <div class="texts-sobre-vantagens">
          <div class="container-sobre-vantagens">
            <div class="text-sobre-vantagens">
              <img src="site/img/icon-calendar.svg" alt="">
              <h4>Acesso exclusivo a eventos</h4>
            </div>
            <p>Descontos nos principais eventos da região e ainda, promoções que vão desde sorteio de ingressos a experiências com artistas.</p>
          </div>
          <div class="container-sobre-vantagens">
            <div class="text-sobre-vantagens">
              <img src="site/img/icon-loudspeaker.svg" alt="">
              <h4>Informação toda a hora</h4>
            </div>
            <p>Acesso ao conteúdo do jornal A Tribuna na versão digital e física, mediante o plano de assinatura.</p>
          </div>
          <div class="container-sobre-vantagens">
            <div class="text-sobre-vantagens">
              <img src="site/img/icon-discount.svg" alt="">
              <h4>Plataforma Digital</h4>
            </div>
            <p>Descontos nos principais eventos da região e ainda, promoções que vão desde sorteio de ingressos a experiências com artistas.</p>
          </div>	
          <div class="button-container">
            <a href="" class="btn --assineSobre">Clique aqui e faça parte do clube</a>
          </div>
        </div>
      </div>
      <div class="col-photo">
        <img class="photo --home" src="site/img/mulher-sobre.png"></div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="component-listagem-blog">
      <div class="listagem-blog --home">
        <div class="swiper swiperBlog --home">
          <div class="swiper-wrapper">
            @foreach ($postsBlog as $post)
            <div class=" --home swiper-slide">
              <div class="card-blog --home">
                <a href="/blog-integra/{{$post->ID}}" class="img-card-blog-container">
                  <img src="{{$post->image}}" alt="{{$post->post_title}}">
                </a>	
                <div class="card-blog-content">
                  <p class="card-blog-data">{{$post->post_date->format('d/m/Y')}}</p>
                  <h3>{{$post->post_title}}</h3>
                  <a href="/blog-integra/{{$post->ID}}"class="btn" >+</a>
                </div>
              </div>
            </div>
            @endforeach
          </div>
          <div class="swiper-pagination"></div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section-economia">
	<div class="container economia-content">
		<div class="col-1">
			<div class="title">
				<img src="site/img/icon-calculator.svg" alt="">
				<h3>Calculadora de Economia <span>Clube A Tribuna</span></h3>
			</div>
			<div class="content">
				<h3>Saiba quanto você pode <span>
          economizar em seu dia-a-dia</span> fazendo parte do Clube A Tribuna!</h3>
				<p>Os valores apresentados tratam-se de estimativas, com base na porcentagem de desconto oferecida aos <span>assinantes do Clube A Tribuna.</span> </p>
        <div class="button-container">
          <a href="" class="btn --assineEconomia">Assine agora!</a>
        </div>
			</div>
		</div>
		<div class="col-2">
			<div class="calculadora-economia">
				<div class="row-1">
					<p>Você economizará:</p>
					<h3 id="calculadora-resultado">R$</h3>
				</div>
				<div class="row-2">
					<p>Durante um mês, qual o valor que você gosta com:</p>
				</div>
				<div class="row-3">
					<div class="calculadora-economia-categoria">
            <div class="categoria-container">
              <img src="site/img/icon-fork.svg" alt="">
              <h3>Idas ao restaurante?</h3>
            </div>
						<div class="input-container">
							<input type="number" id="calc_restaurante" onBlur="calcular()">
							<p>R$</p>
						</div>
					</div>
					<div class="calculadora-economia-categoria">
            <div class="categoria-container">
              <img src="site/img/icon-gas-station.svg" alt="">
              <h3>Abastecimento de veículos com gasolina?</h3>
            </div>
						<div class="input-container">
							<input type="number" id="calc_gasolina" onBlur="calcular()">
							<p>R$</p>
						</div>
					</div>
					<div class="calculadora-economia-categoria">
            <div class="categoria-container">
              <img src="site/img/icon-washing-machine-1.svg" alt="">
              <h3>Utilização de serviços de lavanderia?</h3>
            </div>
						<div class="input-container">
							<input type="number" id="calc_lavanderia" onBlur="calcular()">
							<p>R$</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-slider-descontos">
	<div class="container">
		<div class="slider-descontos-content">
			<div class="text">
				<h2>Grandes empresas <span>Grandes descontos</span></h2>
				<p>Centenas de parceiros para você economizar muito.</p>
			</div>
			<x-slider-descontos></x-slider-descontos>
		</div>
	</div>
</section>

@endsection
