@extends('site.master.layout')

@section('title', 'Descontos')

@section('content')
<x-hero class="bg-purple bg-discount --hero" titleHero="Descontos" titleBreadcrumb="Descontos"></x-hero>
<section class="section-estabelecimentos-conveniados-integra">
  <div class="container estabelecimentos-conveniados-integra-content"> 
		<div class="intro-row">
			<div class="company-wrapper">
				<div class="img-container">
       <img src="{{$post->acf->imagem_titulo->url}}" alt="">
				</div>
				<div class="title-wrapper">
					<h2 class="title">{{$post->post_title}}</h2>
					<p>{{$post->post_excerpt}}</p>
				</div>
			</div>
			<div class="discount-wrapper">
				<div class="discount-content">
					<span>{{$post->acf->desconto}}%</span>
					<p>de desconto</p>
				</div>
				<a href="">Voltar</a>
			</div>
		</div>
		<p class="description">
      {!! $post->post_content !!}
		</p>

		<div class="gallery">
			<div class="swiper sliderDescontosIntegra">
				<div class="swiper-wrapper">
          @for ($i = 1; $i < 7; $i++)
            @php 
              $image = "imagem_galeria_".$i
            @endphp
					<div class="swiper-slide">
						<img src="{{$post->acf->$image->url}}" />
					</div>
          @endfor
				</div>
				<div class="swiper-button-next"></div>
				<div class="swiper-button-prev"></div>
			</div>
			<div thumbsSlider="" class="swiper sliderDescontosIntegraThumb">
      <div class="swiper-wrapper">
          @for ($i = 1; $i < 7; $i++)
            @php 
              $image = "imagem_galeria_".$i
            @endphp
					<div class="swiper-slide">
						<img src="{{$post->acf->$image->url}}" />
					</div>
          @endfor
				</div>
			</div>
		</div>
		<div class="information">
			<div class="usage">
				<h3>Regras de uso</h3>
        <p>{{$post->acf->regra_de_uso}}</p>
        <p>{!! $post->acf->regra_de_uso_observacao !!}</p>
				<a href="">Para acessar o cupom clique aqui</a>
			</div>
			<div class="contact-information">
				<div class="card-information">
					<div class="card-location">
						<h4>Onde estamos</h4>
						<address>{{$post->acf->endereco}}</address>
					</div>
					<div class="card-contact">
						<h4>Fale conosco</h4>
						<div class="telephone-wrapper">
							<img src="" alt="">
							<a href="tel:{{$post->acf->telefone_1}}">{{$post->acf->telefone_1}}</a>
						</div>
						<div class="telephone-wrapper">
							<img src="" alt="">
              <a href="tel:{{$post->acf->telefone_2}}">{{$post->acf->telefone_2}}</a>

						</div>
						<div class="email-wrapper">
							<img src="" alt="">
              <a href="{{$post->acf->website}}">{{$post->acf->website}}</a>
						</div>
					</div>
					<div class="card-socialmedia">
						<div class="icon-img-container">
              <a href="/">							
                <img src="/site/img/icon-facebook-footer.svg" alt="">
              </a>
						</div>
						<div class="icon-img-container">
              <a href="/">	
                <img src="/site/img/icon-instagram-footer.svg" alt="">
              </a>
						</div>
						<p>#acompanhe-nos</p>	
					</div>	
				</div>
			</div>
		</div>
  </div>
</section>
	
@endsection
