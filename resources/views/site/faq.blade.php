@extends('site.master.layout')
@section('title', 'Faq')
@section('subtitle', 'Dúvidas Frequentes')

@section('content')
<x-hero class="bg-purple --faq-hero" titleHero="Faq" titleBreadcrumb="faq" titleFaq=true></x-hero>
  <section class="section-faq"> 
    <div class="faq-content container"> 
    <?php $var=0; ?>
      @foreach ($posts as $post)
      <?php $var++; ?>
        <div class="trigger">
          <span class="counter" id="counter">0.<?php echo $var ?></span>  
          <h4>{{$post->post_title}}</h4>
        </div>
        <div class="content">
          <p>{!! $post->post_content !!}</p>
        </div>
      @endforeach
    </div>
  </section>
@endsection