@extends('site.master.layout')

@section('date', $post->post_date)

@section('content')
<x-hero class="--hero-blog" titleHero="{{$post->post_title}}" titleBreadcrumb="Blog" titleDate="{{date('d/m/Y', strtotime($post->post_date))}}"></x-hero>
<section class="section-blog-integra">
    <div class="img-integra-wrapper">
      <img src="{{$post->acf->imagem_destaque_blog->url}}" alt="">
    </div>
    <div class="blog-integra-content">
      <div class="container">
         <div class="blog-post">
            <div class="blog-post-content">
              <p>{!! $post->post_content !!}</p>  
            </div>
            <div class="blog-share-content">
                <a href="#" class="icon-img-container"> 
                  <img src="/site/img/icon-facebook-footer.svg" alt="Ícone da rede social Facebook">
                </a>
                <a href="#" class="icon-img-container"> 
                    <img src="/site/img/icon-twitter-footer.svg" alt="Ícone da rede social Twitter">
                </a>
                <a href="#" class="icon-img-container"> 
                    <img src="/site/img/icon-instagram-footer.svg" alt="Ícone da rede social Instagram">
                </a>                  
                <p>Compartilhe</p>
            </div>
        </div>
      </div>
       
    </div>
    <div class="line-wrapper ">
        <div class="line container">    
        </div>
    </div>
    <div class="other-posts container">
        <div class="title-wrapper --otherPosts">
            <h2>Leia também <span>Outras notícias</span></h2>
        </div>
        <div class="component-listagem-blog">
          <div class="listagem-blog">
                @foreach ($outrosPosts as $post)
                <div class="card-blog">
                  <div class="img-card-blog-container">
                    <a href="/blog-integra/{{$post->ID}}" >
                      <img src="{{$post->image}}" alt="{{$post->post_title}}">
                    </a>	
                  </div>
                  <div class="card-blog-content">
                    <p class="card-blog-data">{{$post->post_date->format('d/m/Y')}}</p>
                    <h3>{{$post->post_title}}</h3>
                    <a href="/blog-integra/{{$post->ID}}"class="btn" >+</a>
                  </div>
                </div>
                @endforeach
              </div>
        </div>
    
    </div>
</section>


@endsection