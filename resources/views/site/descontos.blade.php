@extends('site.master.layout')

@section('title', 'Descontos')

@section('content')
<x-hero class="bg-purple bg-discount --hero" titleHero="Descontos" titleBreadcrumb="Descontos"></x-hero>
 

<section class="section-listagem-estabelecimentos-conveniados">
  <div class="container estabelecimentos-conveniados-content">
	  <div class="title-wrapper">
			<h2 class="title">Estabelecimentos <span>com descontos</span></h2>
			<form action="/descontos" method="GET">
        <div class="select-wrapper">
					<select name="select" id="">
            <option selected disabled>Categoria</option>
            @foreach($categoria as $itemCategory)
            <option value="{{$itemCategory->slug}}">{{$itemCategory->name}}</option>
          @endforeach 
					</select>
				</div>
				<div class="filter-wrapper">
					<input type="text" name="search" placeholder="Buscar">
					<button type="submit" id="searchsubmit">
          </button>
				</div>
    </form>
		</div>
    <div class="component-listagem-estabelecimentos-conveniados">
      <div class="listagem-estabelecimentos-conveniados">
        @foreach ($posts as $post)
        <div class="card-estabelecimentos">
          <a href="/desconto-integra/{{$post->ID}}" class="img-container">
            <img src="{{$post->image}}" alt="{{$post->post_title}}">
            <p>{{$post->acf->desconto}}%</p>
          </a>
          <div class="content">
            <h3><a href="/desconto-integra/{{$post->ID}}">{{$post->post_title}}</a> </h3>
            <p><a href="/desconto-integra/{{$post->ID}}">{{$post->post_excerpt}}</a></p>
          </div>
          <a href="/desconto-integra/{{$post->ID}}" class="outro">+</a>
        </div>
        @endforeach
      </div>
      <div class="pagination">{{ $posts->links('vendor.pagination.custom') }}</div>
    </div>
  </div>
</section>
	
@endsection
