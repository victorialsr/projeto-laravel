@extends('site.master.layout')
@section('title', 'Contato')
@section('content')

<section class="section-contact">
  <x-hero class="bg-purple --hero" titleHero="Contato" titleBreadcrumb="Contato"></x-hero>
  <div class="row-contact container">
    <div class="form">
      <h2>Fale com o Clube do Assinante A Tribuna</h2>
      <form action="">
        <label for="">Nome:</label>
        <input type="text">
        <div class="row-form">
          <div class="row-email">
            <label for="">E-mail:</label>
            <input type="email" name="" id="">
          </div>
          <div class="row-telephone">
            <label for="">Telefone:</label>
            <input type="tel" name="" id="">
          </div>
        </div>
        <label for="">Mensagem:</label>
        <textarea name="" id="" cols="30" rows="10"></textarea>
        <button class="btn --submit">Enviar</button>
      </form>
    </div>
    <div class="col-photo">
      <div class="photo">  
        <div class="social-media">
          <div class="social-media-content">
            <img src="/site/img/icon-facebook-contact.svg" alt="">
            <a href="">/clubeatribuna</a>
          </div>
          <div class="social-media-content">
            <img src="/site/img/icon-instagram-contact.svg" alt="">
            <a href="">@clubeatribuna</a>
          </div>
          <div class="social-media-content">
            <img src="/site/img/icon-email-contact.svg" alt="">
            <a href="">atendimento@grupo-tribuna.com</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section-location">
  <div class="location-content container">
    <h2>Onde estamos?</h2>
  </div>
  <div class="map-container">
    <div class="container">
      <div class="card-location">
        <img src="site/img/icon-gps.svg" alt="">
        <p>Santos</p>
        <address><span>Rua João Pessoa, 329 - Centro</span>Santos/SP - 11.013-900
        </address>
      </div>
    </div>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3646.6984965667657!2d-46.3263667498921!3d-23.935728284422225!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb0b39f6306c41a0b!2zMjPCsDU2JzA4LjYiUyA0NsKwMTknMjcuMCJX!5e0!3m2!1spt-BR!2sbr!4v1644804902662!5m2!1spt-BR!2sbr" width="100%" height="400px" allowfullscreen="" loading="lazy" class="map-frame"></iframe>
  </div>
  
</section>
@endsection