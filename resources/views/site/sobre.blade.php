@extends('site.master.layout')
@section('title', 'Sobre')
@section('content')
<x-hero class="bg-black --hero" titleHero="Sobre" titleBreadcrumb="Sobre"></x-hero>
<section class="section-vantagens --sobre">
  <div class="vantagens">
    <div class="sobre-main"> 
      <div class="container">
        <div class="onda"></div>
      </div>
      <div class="container content-sobre-main">
        <img src="site/img/logo-clube.png" alt="">
        <div class="text-sobre-main">
          <h3>O Clube</h3>
          <p>O Clube é um programa de relacionamentos com os Assinantes A Tribuna que oferece diversos benefícios para os conveniados A Tribuna.</p>
          <p>Além de muita informação, os assinantes recebem condições exclusivas nas maiores empresas da região além de descontos em shows e espetáculos de teatro.</p>
        </div>
      </div>
    </div>
    <div class="sobre-vantagens container --sobre">
      <div class="content-sobre-vantagens --sobre">
        <div class="title-sobre-vantagens --sobre">	
            <img src="site/img/icon-check.svg" alt="">
            <h3><span>Vantagens</span> de ser do clube</h3>
        </div>
        <div class="texts-sobre-vantagens">
          <div class="container-sobre-vantagens">
            <div class="text-sobre-vantagens">
              <img src="site/img/icon-calendar.svg" alt="">
              <h4>Acesso exclusivo a eventos</h4>
            </div>
            <p>Descontos nos principais eventos da região e ainda, promoções que vão desde sorteio de ingressos a experiências com artistas.</p>
          </div>
          <div class="container-sobre-vantagens">
            <div class="text-sobre-vantagens">
              <img src="site/img/icon-loudspeaker.svg" alt="">
              <h4>Informação toda a hora</h4>
            </div>
            <p>Acesso ao conteúdo do jornal A Tribuna na versão digital e física, mediante o plano de assinatura.</p>
          </div>
          <div class="container-sobre-vantagens">
            <div class="text-sobre-vantagens">
              <img src="site/img/icon-discount.svg" alt="">
              <h4>Plataforma Digital</h4>
            </div>
            <p>Descontos nos principais eventos da região e ainda, promoções que vão desde sorteio de ingressos a experiências com artistas.</p>
          </div>	
          <div class="button-container">
            <a href="" class="btn --assineSobre">Clique aqui e faça parte do clube</a>
          </div>
        </div>
      </div>
      <div class="col-photo --sobre">
        <img src="site/img/mulher-sobre.png" class="photo --sobre"></div>
      </div>
    </div> 
  </div>
</section>
<section class="section-slider-descontos">
	<div class="container">
		<div class="slider-descontos-content">
			<div class="text">
				<h2>Grandes empresas <span>Grandes descontos</span></h2>
				<p>Centenas de parceiros para você economizar muito.</p>
			</div>
			<x-slider-descontos></x-slider-descontos>
		</div>
	</div>
</section>

@endsection
