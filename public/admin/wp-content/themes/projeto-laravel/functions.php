<?php
add_theme_support( 'post-thumbnails' ); 

function descontos_categorias()
{
    register_taxonomy(
        'categorias-descontos',
        'descontos',
        array(
            'labels' => array('name' => 'Categorias'),
            'hierarchical' => true
        )
    );
};
function restaurantes_com_descontos()
{
    register_post_type('Descontos', array(
        'labels' => array('name' => 'Descontos'),
        'public' => true,
        'menu_position' => 0,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
        'menu_icon' => 'dashicons-admin-site'
    ));
};
add_action('init', 'restaurantes_com_descontos');
add_action('init', 'descontos_categorias');

function blog()
{
    register_post_type('blog', array(
        'labels' => array('name' => 'Blog'),
        'public' => true,
        'menu_position' => 0,
        'supports' => array('title', 'editor', 'thumbnail'),
        'menu_icon' => 'dashicons-admin-site'
    ));
};
add_action('init', 'blog');

function blog_categorias()
{
    register_taxonomy(
        'categorias-blog',
        'blog',
        array(
            'labels' => array('name' => 'Categorias'),
            'hierarchical' => true
        )
    );
};
add_action('init', 'blog_categorias');

function faq()
{
    register_post_type('faq', array(
        'labels' => array('name' => 'Faq'),
        'public' => true,
        'menu_position' => 0,
        'supports' => array('title', 'editor'),
        'menu_icon' => 'dashicons-admin-site'
    ));
};
add_action('init', 'faq');

