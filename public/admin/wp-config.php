<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'projeto-laravel' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '|%%eir/MNLU-cS-H;d9FJ~[>BL~p]x]G:6H}g=8<R-v)%y/Yh&dX4r(n1{A!:dlE' );
define( 'SECURE_AUTH_KEY',  '(=ogP(7pa`i?e`:zI]MP%<Gf8K(hJld<6z@PI;MDY~HWtu??tg$+KcLlb>DX7Ha]' );
define( 'LOGGED_IN_KEY',    'HH>mknyR 4+g)sF+TOSQ*^1dV ^@5IigLtu|ip+p}0bh}ND%nk4!Y]$aA52aD];B' );
define( 'NONCE_KEY',        'K/mTc,d4ICS4M;Sj,I_pT{J^.F.zFbY|FzAi_bX&^0GLP YD>P$o/~dP,.th_Ho`' );
define( 'AUTH_SALT',        'i|NF)%yFDNIWKwr@8cIqj8/j}K#[wR-Pkh]=>wEL8*S]T-a|tSc}VIXx~!TEfi&w' );
define( 'SECURE_AUTH_SALT', 'e`8dp_4Z~Wpyh!Z`J2nLcQG+-e/d|,!fEko4Kvi`G,cKDC+)Icz#-b/Nfrgok(w9' );
define( 'LOGGED_IN_SALT',   'rdf0FxX&S5Ry$>B5bHtscT1v/zPgL3r5SnX`ec.6^vk%hB$;vHf;919[.`#4n+qd' );
define( 'NONCE_SALT',       ':XWc6P+!Sr9_=a8O vzF:4miTrkfZvVT,mTk=;?e=mguI:MsZO?R3zGtIadrU[#;' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Adicione valores personalizados entre esta linha até "Isto é tudo". */



/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
