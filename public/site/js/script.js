var swiper = new Swiper(".swiperDescontos", {
    slidesPerView: 3,
    grid: {
        rows: 2,
    },
    spaceBetween: 30,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    breakpoints: {
        300: {
            slidesPerView: 2,
            spaceBetween: 10,
            grid: {
                column: 1,
                rows: 2,
            },
        },
        500: {
            slidesPerView: 3,
            spaceBetween: 10,
            grid: {
                rows: 2,
            },
        },
    },
});

var swiper = new Swiper(".swiperEstabelecimentosConveniados", {
    slidesPerView: 1,
    spaceBetween: 60,
    loop: true,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    breakpoints: {
        768: {
            slidesPerView: 2,
            spaceBetween: 20,
        },

        1200: {
            slidesPerView: 3,
            spaceBetween: 10,
        },
    },
});

var swiper = new Swiper(".swiperBlog", {
    slidesPerView: 1,
    spaceBetween: 30,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
    breakpoints: {
        400: {
            slidesPerView: 1,
            spaceBetween: 20,
        },
        500: {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        1000: {
            slidesPerView: 3,
            spaceBetween: 20,
        },
    },
});

var swiper = new Swiper(".sliderDescontosIntegraThumb", {
    spaceBetween: 15,
    slidesPerView: 3,
    freeMode: true,
    watchSlidesProgress: true,
    breakpoints: {
        700: {
            slidesPerView: 4,
            spaceBetween: 15,
        },
        1000: {
            slidesPerView: 6,
            spaceBetween: 15,
        },
    },
});
var swiper2 = new Swiper(".sliderDescontosIntegra", {
    loop: true,
    spaceBetween: 10,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    thumbs: {
        swiper: swiper,
    },
});

/*hamburguer menu*/
const mobileBtn = document.querySelector("#btn-mobile");
const navLinksToggle = document.querySelectorAll(".menu-link a");

function toggleMenu() {
    const nav = document.querySelector(".nav-menu");
    nav.classList.toggle("active");
}
function toggleLink() {
    navLinksToggle.forEach((navLink) => {
        navLink.addEventListener("click", toggleMenu);
    });
}
mobileBtn.addEventListener("click", toggleMenu);
toggleLink();
/*hamburguer menu*/

function calcular() {
    var num1 = Number(document.getElementById("calc_restaurante").value);
    var num2 = Number(document.getElementById("calc_gasolina").value);
    var num3 = Number(document.getElementById("calc_lavanderia").value);
    var elemResult = document.getElementById("calculadora-resultado");
    num1 = num1 * 0.2;
    num2 = num2 * 0.05;
    num3 = num3 * 0.2;
    total = (num1 + num2 + num3).toFixed(2);
    elemResult.innerText = "R$" + String(total);
}

const trigger = document.querySelectorAll(".trigger");
const content = document.querySelectorAll(".content");

window.addEventListener("load", (event) => {
    content.forEach((el, index) => {
        const height = el.scrollHeight + 250;
        el.style.setProperty("--max-height", height + "px");
    });
});

trigger.forEach((el, index) => {
    el.addEventListener("click", (event) => {
        event.target.classList.toggle("is-open");
        event.target.nextElementSibling.classList.toggle("is-open");
    });
});
