<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ListagemDescontos;
use App\Http\Controllers\ListagemFaq;
use App\Http\Controllers\SearchController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'postsIndex'])->name('site.home');

Route::get('/descontos', [ListagemDescontos::class, 'descontos'])->name('site.descontos');

Route::get('/blog-integra/{id}', [PostController::class, 'blogIntegra'])->name('site.blog-integra');

Route::get('/blog', [PostController::class, 'blog'])->name('site.blog');

Route::get('/contato', function () {
  return view('site.contato');
})->name('site.contato');

Route::get('/desconto-integra/{id}', [ListagemDescontos::class, 'descontoIntegra'])->name('site.desconto-integra');

Route::get('/faq', [ListagemFaq::class, 'faq'])->name('site.faq');

Route::get('/parceiros', function () {
  return view('site.parceiros');
})->name('site.parceiros');

Route::get('/sobre', function () {
  return view('site.sobre');
})->name('site.sobre');

